﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card")]
public class Card : ScriptableObject
{
    public string cardName;
    public bool isOperator;  // Use boolean to determine if the card is an operator or number
    public Sprite art;
    public int cardValue;
    public string operation;

}

