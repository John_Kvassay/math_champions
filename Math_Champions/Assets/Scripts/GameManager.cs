﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    
    public static GameManager instance = null;
        public int playerPowerLevel = 0; // The total power of the players cards 
        public float enemyPowerLevel = 0; // The tatal Power of the enemy player
        public GameObject AdditionCard;
        public List<int> activeCardValues = new List<int>(); // creates a list of all the values of the active number cards in play
        public Transform playArea;
        public Text playerPowerLevelDisplay;
        public Text enemyPowerLevelDisplay;

        public GameObject youWinText;
        public GameObject enemyWinText;
        
    void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }


    }


    void Start()
    {
        enemyPowerLevel = Random.Range(1, 4);

    }

    // Update is called once per frame
	void Update ()
    {

        playerPowerLevelDisplay.text = playerPowerLevel.ToString("F0");
        enemyPowerLevelDisplay.text = enemyPowerLevel.ToString("F0");

        // Spawns the addition card
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
            //Instantiate(AdditionCard,transform.position,transform.rotation);
           
        //}

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            loadMainMenu();
        }
        

        //Debug.Log(playerPowerLevel);
      
	}


    public void endround()
    {
        if (playerPowerLevel > enemyPowerLevel)
        {
            youWinText.SetActive(true);
        }
        else
        {
            enemyWinText.SetActive(true);
        }
    }

    public void quitGame()
    {
        Application.Quit();
    }


    public void loadgame()
    {
        SceneManager.LoadScene("Main");
    }

    public void loadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
