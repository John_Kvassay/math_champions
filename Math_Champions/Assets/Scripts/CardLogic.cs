﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class CardLogic : MonoBehaviour
{
    //public Text title;
    //public Image art;

    
    public Card card;
    
    void Start()
    {
    
    }

    public void runCardLogic ()
    {
        if (card.isOperator == false)
        {
            GameManager.instance.activeCardValues.Add(card.cardValue);
            // Takes the value of the card and adds it to the list
            Debug.Log("Added");
        }


        // Checks to see if the isOperator is set to true
        if (card.isOperator == true)
        {

            if (card.operation == "+")
            {
                GameManager.instance.playerPowerLevel = GameManager.instance.activeCardValues[0] + GameManager.instance.activeCardValues[1];

                Debug.Log("Evaluated");
            }
        }

        getCard(card);
    }

    public void getCard(Card c)
    {
        if (c == null)
        {
            return;
        }
    }
}


