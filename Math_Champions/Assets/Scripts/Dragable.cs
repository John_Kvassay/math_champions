﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Dragable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public Transform parentToAttachTo = null;

    public GameObject cd;
    // Variable that defines the play area
    public Transform playArea;
    public CardLogic cardLogic;

    public void OnBeginDrag(PointerEventData eventdata)
    {
        parentToAttachTo = this.transform.parent;
        this.transform.SetParent(this.transform.parent.parent);

        GetComponent<CanvasGroup>().blocksRaycasts = false;

        // Set the currently dragged card to cd

    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.SetParent(parentToAttachTo);
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        // Checks to see if the card is in the play area.
        // If the card is in the play area then it will allow the card logic to run.  
        if (parentToAttachTo == playArea)
        {
            // Grab the Card Logic component off the current card (the one this script is on!)
            cardLogic = gameObject.GetComponent<CardLogic>();

            // Runs the runCardLogic function in the cardlogic script
            cardLogic.runCardLogic();

            //.cardinplay = 1;  
            Debug.Log("Card is in Play");
        }
    }
}


   
