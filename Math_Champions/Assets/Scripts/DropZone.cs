﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class DropZone : MonoBehaviour, IDropHandler {

    public void OnDrop(PointerEventData eventData) {
        //Debug.Log(eventData.pointerDrag.name + " was dropped on " + gameObject.name);

        Dragable d = eventData.pointerDrag.GetComponent<Dragable>();
        if (d != null)
        {
            d.parentToAttachTo = this.transform;
        }
    }
}
